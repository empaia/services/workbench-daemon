# 0.2.23

* renovate

# 0.2.22

* renovate

# 0.2.21

* renovate

# 0.2.20

* add organization header

# 0.2.19

* renovate

# 0.2.18

* renovate

# 0.2.17

* renovate

# 0.2.16

* renovate

# 0.2.15

* renovate

# 0.2.14

* renovate

# 0.2.13

* renovate

# 0.2.12

* renovate

# 0.2.11

* renovate

# 0.2.10

* renovate

# 0.2.9

* use human readable and generic internal error message

# 0.2.8

* renovate

# 0.2.7

* renovate

# 0.2.6

* renovate

# 0.2.5

* renovate

# 0.2.4

* renovate

# 0.2.3

* updated ci

# 0.2.2

* bugfix

# 0.2.1

* bugfix

# 0.2.0

* adapted to JES execution model changes
* refactored into smaller functions

# 0.1.8

* removed automatic client sub determination

# 0.1.7

* Bugfix orignated from 0.1.6

# 0.1.6

* If a JES Execution is COMPLETED but the JS status is not (e.g. App does not call finalize), then the job is set to FAILED at JS

# 0.1.5

* fix "user_id" header to JES, which should be "user-id" (no underscore)

# v0.1.4

* added PyJWT and cryptography (needed for 0.1.3 feature)

# v0.1.3

* sending user_id header with requests to JES

# v0.1.2

* updated sender-auth

# v0.1.1

* fixed auth_settings
