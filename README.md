# Workbench Daemon

## Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`

```bash
poetry shell
```

```bash
black .
isort .
pycodestyle workbench_daemon
pylint workbench_daemon
```
