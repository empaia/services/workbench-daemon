FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.1.34@sha256:a72030518b45ed752822800c03c27d66f14bc4e960b8677da3f770b1eb0ce688 AS builder
COPY . /workbench_daemon
WORKDIR /workbench_daemon
RUN poetry build && poetry export -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.1.34@sha256:27b5621678d4966bf2d7d7c4462cbbf4826ff072f39db122232f8a19e8c732db
COPY --from=builder /workbench_daemon/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /workbench_daemon/dist /artifacts
RUN pip install /artifacts/*.whl
ENV PYTHONUNBUFFERED=1
CMD ["wbd"]