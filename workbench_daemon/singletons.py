import logging

from .empaia_sender_auth import AioHttpClient, AuthSettings
from .settings import Settings

settings = Settings()

logging.basicConfig()
if settings.debug:
    logging.root.setLevel(logging.DEBUG)
else:
    logging.root.setLevel(logging.INFO)
logger = logging.getLogger("WBD")

auth_settings = None
if settings.idp_url is not None:
    auth_settings = AuthSettings(
        idp_url=settings.idp_url, client_id=settings.client_id, client_secret=settings.client_secret
    )
    logger.info("Auth enabled.")
else:
    logger.info("Auth disabled.")

http_client = AioHttpClient(logger=logger, auth_settings=auth_settings, enable_custom_mode=True)
