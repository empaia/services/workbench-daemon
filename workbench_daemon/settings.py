from pydantic import BaseSettings


class Settings(BaseSettings):
    mds_url: str
    jes_url: str
    poll_interval: int = 5
    job_ready_trials: int = 5
    debug: bool = False
    idp_url: str = None
    organization_id: str = "dummy_org"
    client_id: str = None
    client_secret: str = None

    class Config:
        env_file = ".env"
        env_prefix = "wbd_"
