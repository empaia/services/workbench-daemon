import asyncio
from collections import Counter

from aiohttp.client_exceptions import ClientConnectorError, ClientOSError

from .empaia_sender_auth import CustomHTTPException
from .singletons import http_client, logger, settings

CLIENT_EXCEPTIONS = (ClientConnectorError, ClientOSError, CustomHTTPException)


def _http_exception_log(e):
    if isinstance(e, CustomHTTPException):
        logger.debug(f"{e.status_code}: {e.detail}")


def _log_exception_info(e, additional_message):
    logger.info(f"{type(e).__name__} {additional_message}")


def _jobs_ready(jobs):
    return filter(lambda job: job["status"] == "READY", jobs)


def _jobs_not_ready(jobs):
    return filter(lambda job: job["status"] != "READY", jobs)


async def _update_job_status(job_id, status, error_message):
    data = {"status": status, "error_message": error_message}
    return await http_client.put(f"{settings.mds_url}/v1/jobs/{job_id}/status", json=data)


async def _get_execution(job_id):
    return await http_client.get(
        f"{settings.jes_url}/v1/executions/{job_id}", headers={"organization-id": settings.organization_id}
    )


async def _fetch_jobs(statuses):
    jobs_object = await http_client.put(f"{settings.mds_url}/v1/jobs/query", json={"statuses": statuses})
    return jobs_object["items"]


async def _check_jobs_ready_for_too_long(jobs, job_ready_counter):
    for job in _jobs_ready(jobs):
        job_id = job["id"]
        job_ready_counter.update([job_id])

        if job_ready_counter[job_id] < settings.job_ready_trials:
            continue

        # Start failure handling
        job_ready_counter.pop(job_id)

        execution = None
        try:
            execution = await _get_execution(job_id)
        except CLIENT_EXCEPTIONS as e:
            _log_exception_info(e, f"when retrieving execution for READY job {job_id} from JES.")
            _http_exception_log(e)

        if execution is None:
            try:
                error_message = f"WBD detected job {job_id} in status READY for too long."
                await _update_job_status(job_id, "ERROR", error_message)
                logger.debug(error_message)
            except CLIENT_EXCEPTIONS as e:
                _log_exception_info(e, "when updating READY job not in JES.")
                _http_exception_log(e)
            continue

        jes_message = execution.get("message")
        jes_status = execution["status"]
        error_message = (
            f"WBD detected job in status READY for too long. JES status: {jes_status}; JES message: {jes_message}"
        )
        try:
            await _update_job_status(job_id, "ERROR", error_message)
            logger.debug(error_message)
        except CLIENT_EXCEPTIONS as e:
            _log_exception_info(e, f"when updating READY job {job_id} from JES status.")
            _http_exception_log(e)


def _remove_non_ready_jobs_from_ready_counter(jobs, job_ready_counter):
    ready_job_ids = map(lambda job: job["id"], _jobs_ready(jobs))
    for job_id in list(job_ready_counter):
        if job_id not in ready_job_ids:
            job_ready_counter.pop(job_id)


async def _update_state_for_jobs_not_in_ready_state(jobs):
    for job in _jobs_not_ready(jobs):
        job_id = job["id"]
        try:
            execution = await _get_execution(job_id)
        except CLIENT_EXCEPTIONS as e:
            _log_exception_info(e, f"when retrieving execution for non READY job {job_id} from JES.")
            _http_exception_log(e)
            # handle this case, or keep trying?
            continue

        if execution["status"] == job["status"]:  # can be true for SCHEDULED or RUNNING
            continue

        terminal_state_messages = {
            "TERMINATED": "App terminated before finalizing its computation",
            "STOPPED": "App computation was stopped on request",
            "TIMEOUT": "App computation was stopped due to timeout",
            "ERROR": "Internal execution error, please report a bug",
        }

        if execution["status"] in terminal_state_messages:
            job_status = "ERROR"
            job_error_message = terminal_state_messages[execution["status"]]
        else:
            job_status = execution["status"]
            job_error_message = None

        try:
            # since terminal job statuses can't be overridden we can unconditionally try to update it
            # if the app already called /finalized or /failure this call will just be rejected
            # otherwise the job did neither do an explict call to /finalize nor /failure and has to be
            # updated accordintly
            await _update_job_status(job_id, job_status, job_error_message)
            logger.debug(f"Update job {job_id} state to {job_status}")
        except CLIENT_EXCEPTIONS as e:
            _log_exception_info(
                e,
                f"when updating status for non READY job {job_id} (not necessarily an error for terminal statuses).",
            )
            _http_exception_log(e)


async def run_async():
    job_ready_counter = Counter()
    while True:
        logger.debug(f"Sleep {settings.poll_interval} seconds...")
        await asyncio.sleep(settings.poll_interval)

        # GENERAL: strategy for robustness and failure recovery is, to always fetch all jobs with certain status
        # then think of all cases that can occur with a job in a certain state
        # this includes, that we have to think of cases that occur when unforseen errors happen

        try:
            jobs = await _fetch_jobs(["READY", "SCHEDULED", "RUNNING"])
        except CLIENT_EXCEPTIONS as e:
            _log_exception_info(e, "when retrieving jobs from MDS.")
            _http_exception_log(e)
            continue
        await _check_jobs_ready_for_too_long(jobs, job_ready_counter)
        _remove_non_ready_jobs_from_ready_counter(jobs, job_ready_counter)
        await _update_state_for_jobs_not_in_ready_state(jobs)


def run():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_async())
